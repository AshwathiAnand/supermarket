/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supermarket.dao;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ashwa
 */
//Here CommonDao.java page's interface implementations are done.
//For each operation a corresponding session. ie., a small amount of memory is maintained by sessionFactory
@Repository("CommonDao") //For object creation using @Autowired
@Component

public class User_dao_impl implements CommonDao {

    @Autowired  //only once object creation is possible
    private SessionFactory sessionFactory;

    @Override
    @Transactional  //when connecting with db
    public void save(Object obj) {  //To save bean to db
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.clear();
        currentSession.saveOrUpdate(obj);
    }

    @Override
    @Transactional
    public List listAll(Class obj) {    //In list duplicate data allowed.
        Session currentSession = sessionFactory.getCurrentSession();
        Criteria createCriteria = currentSession.createCriteria(obj);
        createCriteria.setLockMode(LockMode.NONE);
        return createCriteria.list();
    }

    @Override
    @Transactional
    public Object getBeanById(Class obj, Integer id) {
        Session currentSession = sessionFactory.getCurrentSession();
        return currentSession.get(obj, id, LockMode.NONE);
    }

    @Override
    @Transactional
    public Object update(Object obj) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.update(obj);
        return obj;
    }

    @Override
    @Transactional
    public void delete(Object obj) {
        Session currentSession = sessionFactory.getCurrentSession();
        currentSession.delete(obj);

    }

    @Override
    @Transactional
    public Object getUniqueObject(Class info, String[] keys, Object... values) {
        Session currentSession = sessionFactory.getCurrentSession();
        Criteria createCriteria = currentSession.createCriteria(info);
        createCriteria.setLockMode(LockMode.NONE);
        if (keys != null && values != null && keys.length == values.length) {
            int i = 0;
            for (Object v : values) {
                createCriteria.add(Restrictions.eq(keys[i++], v));
            }
        }
        return createCriteria.uniqueResult();
    }
}
