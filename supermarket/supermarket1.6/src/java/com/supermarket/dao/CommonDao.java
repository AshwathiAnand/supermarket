/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supermarket.dao;

import java.util.List;

/**
 *
 * @author ashwa
 */
//This is the interface which deals with database operations needed for our project. 
//Its implementations are done at User_dao_impl.java page
public interface CommonDao {
    public void save (Object obj);
    public List listAll(Class info);
    public Object getBeanById(Class info, Integer value);
    public Object update(Object info);
    public void delete(Object obj);
    public Object getUniqueObject(Class info, String[] keys, Object... values);
 }
