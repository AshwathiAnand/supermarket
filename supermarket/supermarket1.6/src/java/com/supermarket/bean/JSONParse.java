package com.supermarket.bean;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 * object to JSON conversion.
 * Using Interface,  in-order to nullify object creation concept in interface use the keyword 'default'.
 *  which makes object creation possible from java API 8. Also makes all fields access-modifiers public ,whether it 
 * is of private or protected.
 * By using JSONParse interface we can accept any class object and can easily convert it into JSONObject
 * 
 * @author ashwa
 */
public interface JSONParse {

    default JSONObject toJSON() {
        JSONObject onj = new JSONObject();
        Field[] fields = getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                onj.put(field.getName(), field.get(this));
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(JSONParse.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(JSONParse.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return onj;
    }
}
