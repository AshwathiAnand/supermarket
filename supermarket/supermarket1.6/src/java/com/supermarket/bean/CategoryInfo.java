/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supermarket.bean;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category")  //To set table constraints. ie., table name as category and pk as name )
public class CategoryInfo implements Serializable {

    private static final long serialVersionUID = 2L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id; //here field id is auto incremented using @GeneratedValue

    @Column(name = "name")
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "categoryInfo",fetch = FetchType.EAGER, cascade = CascadeType.ALL)//when cascade given in parent.wont remove parent entry while deleting child
    private List<ProductInfo> product;

    public List<ProductInfo> getProduct() {
        return product;
    }

    public void setProduct(List<ProductInfo> product) {
        this.product = product;
    }

   
}
