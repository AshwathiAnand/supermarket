/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supermarket.bean;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ashwa
 */
@Entity //To create a table
@Table(name = "login", uniqueConstraints = {
    @UniqueConstraint(columnNames = "email") 
})  //To set table constraints. ie., table name as login and pk as email 
public class LoginInfo implements Serializable,JSONParse {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id; //here field id is auto incremented using @GeneratedValue
    
    //@Column(name ="email")
    private String email;    //Here email field value attributes are automatically set.
       
    @Column(name = "password", nullable = false)
    private String password;
    
    private String type;

    @OneToOne(mappedBy = "login")  //parent class .ie. mapped to login table
    private UserInfo user;  //child class ref. var
    
    //returns user
    public UserInfo getUser(){
        return  user;
    }
    
    //set user
    public void setUser(UserInfo user){
        this.user = user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

     public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
