/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supermarket.bean;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity //to create a Table entity 
@Table(name = "customer_info")  //table name customer info
public class UserInfo implements Serializable {
    
    @Id
    @GeneratedValue(strategy  = GenerationType.IDENTITY) //to convert id field to PK
    private int id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "phone")
    private String phone;
    
    @Column(name = "gender")
    private String gender;
    
    private String address;
    
    //To give foreign key constraints
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)//cascade - a change here reflects change in parent
    @JoinColumn(name = "user_id", referencedColumnName = "id")//user_id field of custumer_info table refers to id field in login table
    private LoginInfo login;
    
    public LoginInfo getLoginInfo() {
        return login;
    }
    
    public void setLoginInfo(LoginInfo login) {
        this.login = login;
    }
//    public LoginInfo getLogin() {
//        return login;
//    }
//
//    public void setLogin(LoginInfo login) {
//        this.login = login;
//    }  

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
 
 }
