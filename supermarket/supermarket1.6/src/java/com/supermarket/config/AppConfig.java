/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supermarket.config;

import java.util.Properties;
import javax.servlet.MultipartConfigElement;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.support.StandardServletMultipartResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 *
 * @author ashwa
 */
//Annotations are used for understanding spring framework that it is  of its components.
//@ symbol is followed by the component name
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.supermarket.*")
@EnableTransactionManagement

public class AppConfig extends WebMvcConfigurerAdapter{
    
    @Autowired
    private SessionFactory sessionFactory;
  
    @Bean(name = "resolver") 
    public ViewResolver viewResolver(){
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/jsp/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }
    
    @Autowired
    DataSource dataSource;
    
    @Bean
    public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate(){
        return new NamedParameterJdbcTemplate(dataSource);
    }
    
    @Bean
    public DataSource getDataSource() {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl("jdbc:mysql://localhost/supermarket");
        ds.setUsername("root");
        ds.setPassword("mysql");
        return ds;
    }
    
    @Bean
    public SessionFactory getSessionFactory(DataSource source){
        LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(source);
        builder.scanPackages("com.supermarket.bean");
        builder.addProperties(getHibernateSchemaProperty());
        SessionFactory sessionfactory = builder.buildSessionFactory();
        return sessionfactory;
        
    }
    
    @Bean
    @Autowired
    public HibernateTransactionManager getTransaction(SessionFactory sessionFactory) {
        HibernateTransactionManager htm = new HibernateTransactionManager();
        htm.setSessionFactory(sessionFactory);
        return htm;
    }
    public Properties getHibernateSchemaProperty() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");//To conect with mysql
        properties.put("hibernate.show_mysql", "true");//For activating Table in mysql
        properties.put("hibernate.hbm2ddl.auto", "update"); //for creating table if not present. OW update table       
        return properties;
    }  
    @Bean
    public MultipartResolver multipartResolver() {  //needed for uploading photos
        return new StandardServletMultipartResolver();
    }
   
    @Bean
    public MultipartConfigElement multipartConfigElement(){
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("1024MB");
        factory.setMaxRequestSize("128MB");
        return  factory.createMultipartConfig();
    }
    
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry){
       registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }
    
}

