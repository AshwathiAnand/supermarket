/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supermarket.controller;

import com.supermarket.bean.CategoryInfo;
import com.supermarket.bean.ProductInfo;
import com.supermarket.dao.CommonDao;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ashwa
 */
@Controller
public class AdminController {
    
    private String TMP_FOLDER = "resources/tmp";
    @Autowired
    CommonDao comDao;
    
    @RequestMapping(value = "viewcategory", method = RequestMethod.GET) //To view add category page
    public String viewCategory(@ModelAttribute("cato") CategoryInfo category, SessionStatus status) {
        if (status.isComplete()) {
            return "index";
        }
        return "adminAddCategory";
    }
    
    @RequestMapping(value = "addcategory", method = RequestMethod.POST) //For adding category
    public String addCategory(@ModelAttribute("cato") CategoryInfo category, ModelMap model) {
        comDao.save(category);
        model.addAttribute("message", "Category Added Successfully");
        return "adminAddCategory";
    }

    @RequestMapping(value = "categorylist", method = RequestMethod.GET)//For displaying category list
    public ModelAndView viewCategorylist(SessionStatus status) {
        ModelAndView model = new ModelAndView("adminViewCategory");//name of page to go 
        System.out.println(status.isComplete());
        if (status.isComplete()) {
            model.setViewName("index");
        }
        List listAll = comDao.listAll(CategoryInfo.class);
        
        model.addObject("categorylist", listAll);   //adds list to model//For using with expression tag
        return model;  //returns category list
    }
    
    @RequestMapping(value = "viewproduct", method = RequestMethod.GET) //To view add product page
    public ModelAndView viewProduct(@ModelAttribute("prod") ProductInfo productInfo, SessionStatus status) {
        ModelAndView model = new ModelAndView("adminAddProduct");
        List listAll = comDao.listAll(CategoryInfo.class);
        model.addObject("categorylist_in_product", listAll);   //adds list to model//For using with expression tag

        if (status.isComplete()) {
            model.setViewName("index");
            return model;
        }
        
        return model;
    }
    
    @RequestMapping(value = "addproduct", method = RequestMethod.POST) //For adding product
    public String addProduct(@ModelAttribute("pro") ProductInfo productInfo, @ModelAttribute("cat") CategoryInfo categoryInfo, ModelMap model, @RequestParam("file") MultipartFile multipartFile) {
        ServletContext servletContext = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
                .getRequest().getServletContext();  //To get content from Apache to that of ours context
        String uploadPath = servletContext.getRealPath("") + File.separator + TMP_FOLDER;
        File fileF = new File(uploadPath + File.separator + productInfo.getName());//makes folder in users name
        fileF.mkdirs();//create Folder
        if (!multipartFile.isEmpty()) {
            try {
                FileOutputStream fos = new FileOutputStream(fileF + File.separator + multipartFile.getOriginalFilename());
                fos.write(multipartFile.getBytes());
                fos.flush();
                fos.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, ex);
            }
            categoryInfo = (CategoryInfo) comDao.getBeanById(CategoryInfo.class, categoryInfo.getId());
            
            System.out.println(categoryInfo.getName()); //?
            productInfo.setCategoryInfo(categoryInfo);
            productInfo.setPhoto(multipartFile.getOriginalFilename());
            productInfo.setId(0);
           
            comDao.save(productInfo);
            model.addAttribute("message", "Product Added Successfully");
        } else {
            model.addAttribute("message", "Failed to add product");
        }
        return "adminAddProduct";
    }
    
    @RequestMapping(value = "productlist", method = RequestMethod.GET)//For displaying product list
    public ModelAndView viewProductlist(SessionStatus status) {
        ModelAndView model = new ModelAndView("adminViewProducts");//name of page to go 
        System.out.println(status.isComplete());    //?
        if (status.isComplete()) {
            model.setViewName("index");
        }
        List listAll = comDao.listAll(ProductInfo.class);
        model.addObject("productlist", listAll);   //adds list to model//For using with expression tag
        return model;  //returns product list
    }
       
    @RequestMapping(value = "deleteproduct", method = RequestMethod.GET)
    public ModelAndView deleteProduct(@RequestParam String id) {
        ProductInfo obj = (ProductInfo) comDao.getBeanById(ProductInfo.class, Integer.parseInt(id));
        if(obj!=null)
            comDao.delete(obj);
        List listAll = comDao.listAll(ProductInfo.class);
        ModelAndView model = new ModelAndView("adminViewProducts"); //return page
        model.addObject("productlist", listAll);
        return model;
    }
     
    @RequestMapping(value = "editproduct", method = RequestMethod.GET)
    public ModelAndView editProduct(@RequestParam String id) {
        Object obj = comDao.getBeanById(ProductInfo.class, Integer.parseInt(id));
        List<CategoryInfo> listAll = comDao.listAll(CategoryInfo.class);
        ModelAndView model = new ModelAndView("adminEditProduct");
        model.addObject("editproducts", obj);   //add obj to model
        model.addObject("category", listAll);   //add obj to model
        return model;
    }
    
    @RequestMapping(value = "updateproduct", method = RequestMethod.POST)
    public ModelAndView updateProduct(@ModelAttribute("product") ProductInfo productInfo, @ModelAttribute("category") CategoryInfo categoryInfo, @RequestParam("file") MultipartFile file) {

        ServletContext servletContext = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getServletContext();  //To get content from Apache to that of ours context
        String uploadPath = servletContext.getRealPath("") + File.separator + TMP_FOLDER;
        File fileF = new File(uploadPath + File.separator + productInfo.getName());
        fileF.mkdirs();
        if (!file.isEmpty()) {
            try {
                FileOutputStream oos = new FileOutputStream(fileF + File.separator + file.getOriginalFilename());
                oos.write(file.getBytes());
                oos.close();
               
                ProductInfo product = (ProductInfo) comDao.getBeanById(ProductInfo.class, productInfo.getId());
                productInfo.setCategoryInfo(categoryInfo);
                comDao.update(product);
               
            } catch (FileNotFoundException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("id = " + productInfo.getId());
            ProductInfo productInfo1 = (ProductInfo) comDao.getBeanById(ProductInfo.class, productInfo.getId());
            productInfo1.setCategoryInfo(categoryInfo);
            comDao.update(productInfo1);
            //categoryInfo.setProduct(categoryInfo); //for viewing..values are set in parent.here it is login
        }
        ModelAndView model = new ModelAndView("adminEditProduct");
        model.addObject("product", productInfo);
        return model;
    }
//    private String TMP_FOLDER = "resources/tmp";
//    @Autowired
//    CommonDao comDao;
//
//    @RequestMapping(value = "/saveuser", method = RequestMethod.POST)
//    public String save(@ModelAttribute("login") LoginInfo login,
//            @ModelAttribute("user") UserInfo user, ModelMap model) {
//        
////        ServletContext servletContext = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
////                .getRequest().getServletContext();  //To get content from Apache to that of ours context
////        String uploadPath = servletContext.getRealPath("") + File.separator + TMP_FOLDER;
////        File fileF = new File(uploadPath + File.separator + login.getUsername());//makes folder in users name
////        fileF.mkdirs();//create Folder
////        
//                user.setLoginInfo(login);   //While saving parent class object is taken to that of child class
//                comDao.save(user);  //To save to db. Here CommonDao interface method is called
//                model.addAttribute("message", "Saved this Request" + user.getName());  //To get message to jsp page
//                                    //Here "message" is the word used in register.jsp page.There at <h1> tag we
//                                    //wants to display "Saved this Request"   
//        return "index";
//    }
//    //   @RequestMapping (value = "/user" , method = RequestMethod.GET)
////    public String save(@RequestParam("name") String name,@RequestParam("username") String username, ModelMap model) {
////        
////        System.out.println("name :"+name);
////        return "register";
////    }
//
//    @RequestMapping(value = "userslist", method = RequestMethod.GET)
//    public ModelAndView viewUsers() {
//        List listAll = comDao.listAll(LoginInfo.class);
//        ModelAndView model = new ModelAndView("userslist"); //For using with expression tag
//        model.addObject("userslist", listAll);   //adds list to model
//        return model;  //returns users list
//    }
//
//    @RequestMapping(value = "edit", method = RequestMethod.GET)
//    public ModelAndView editUsers(@RequestParam String id) {
//        Object obj = comDao.getBeanById(LoginInfo.class, Integer.parseInt(id));
//        ModelAndView model = new ModelAndView("edit");
//        model.addObject("user", obj);   //add obj to model
//        return model;
//    }
//
//    @RequestMapping(value = "update", method = RequestMethod.POST)
//    public ModelAndView updateUsers(@ModelAttribute("user") UserInfo user, @ModelAttribute("login") LoginInfo login, @RequestParam("file") MultipartFile file) {
//
//        ServletContext servletContext = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getServletContext();  //To get content from Apache to that of ours context
//        String uploadPath = servletContext.getRealPath("") + File.separator + TMP_FOLDER;
//        File fileF = new File(uploadPath + File.separator + login.getUsername());
//        fileF.mkdirs();
//        if (!file.isEmpty()) {
//            try {
//                FileOutputStream oos = new FileOutputStream(fileF + File.separator + file.getOriginalFilename());
//                oos.write(file.getBytes());
//                oos.close();
//               
//                LoginInfo lb = (LoginInfo) comDao.getBeanById(LoginInfo.class, login.getId());
//                user.setLoginInfo(lb);
//                comDao.update(user);
//                login.setUser(user);
//            } catch (FileNotFoundException ex) {
//                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
//            } catch (IOException ex) {
//                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        } else {
//            System.out.println("id = " + login.getId());
//            LoginInfo lb = (LoginInfo) comDao.getBeanById(LoginInfo.class, login.getId());
//            user.setLoginInfo(lb);
//            comDao.update(user);
//            login.setUser(user); //for viewing..values are set in parent.here it is login
//        }
//        ModelAndView model = new ModelAndView("edit");
//        model.addObject("user", login);
//        return model;
//    }
//    
//    @RequestMapping(value = "delete", method = RequestMethod.GET)
//    public ModelAndView deleteUsers(@RequestParam String id) {
//        LoginInfo obj = (LoginInfo) comDao.getBeanById(LoginInfo.class, Integer.parseInt(id));
//        if(obj!=null)
//            comDao.delete(obj.getUser());
//        List listAll = comDao.listAll(LoginInfo.class);
//        ModelAndView model = new ModelAndView("userslist"); //return page
//        model.addObject("userslist", listAll);
//        return model;
//    }
}
