/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supermarket.controller;

import com.supermarket.bean.LoginInfo;
import com.supermarket.bean.UserInfo;
import com.supermarket.dao.CommonDao;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ashwa
 */
@Controller
public class UserController {

    private String TMP_FOLDER = "resources/tmp";
   
    @Autowired
    CommonDao comDao;
    @RequestMapping(value = "/saveuser", method = RequestMethod.POST)
    public String save(@ModelAttribute("login") LoginInfo login,
            @ModelAttribute("user") UserInfo user, ModelMap model) {
        
                user.setLoginInfo(login);   //While saving parent class object is taken to that of child class
                comDao.save(user);  //To save to db. Here CommonDao interface method is called
                model.addAttribute("message", "Saved this Request" + user.getName());  //To get message to jsp page
                                    //Here "message" is the word used in register.jsp page.There at <h1> tag we
                                  //wants to display "Saved this Request"  
//        if(user.getName()== "admin")                           
//            return "admin_Home";
//        else
            return "index";
    }
    //   @RequestMapping (value = "/user" , method = RequestMethod.GET)
//    public String save(@RequestParam("name") String name,@RequestParam("username") String username, ModelMap model) {
//        
//        System.out.println("name :"+name);
//        return "register";
//    }

    @RequestMapping(value = "userslist", method = RequestMethod.GET)
    public ModelAndView viewUsers() {
        List listAll = comDao.listAll(LoginInfo.class);
        ModelAndView model = new ModelAndView("userslist"); //For using with expression tag
        model.addObject("userslist", listAll);   //adds list to model
        return model;  //returns users list
    }

    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public ModelAndView editUsers(@RequestParam String id) {
        Object obj = comDao.getBeanById(LoginInfo.class, Integer.parseInt(id));
        ModelAndView model = new ModelAndView("edit");
        model.addObject("user", obj);   //add obj to model
        return model;
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    public ModelAndView updateUsers(@ModelAttribute("user") UserInfo user, @ModelAttribute("login") LoginInfo login, @RequestParam("file") MultipartFile file) {

        ServletContext servletContext = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getServletContext();  //To get content from Apache to that of ours context
        String uploadPath = servletContext.getRealPath("") + File.separator + TMP_FOLDER;
        File fileF = new File(uploadPath + File.separator + user.getName());
        fileF.mkdirs();
        if (!file.isEmpty()) {
            try {
                FileOutputStream oos = new FileOutputStream(fileF + File.separator + file.getOriginalFilename());
                oos.write(file.getBytes());
                oos.close();
               
                LoginInfo lb = (LoginInfo) comDao.getBeanById(LoginInfo.class, login.getId());
                user.setLoginInfo(lb);
                comDao.update(user);
                login.setUser(user);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.println("id = " + login.getId());
            LoginInfo lb = (LoginInfo) comDao.getBeanById(LoginInfo.class, login.getId());
            user.setLoginInfo(lb);
            comDao.update(user);
            login.setUser(user); //for viewing..values are set in parent.here it is login
        }
        ModelAndView model = new ModelAndView("edit");
        model.addObject("user", login);
        return model;
    }
    
    @RequestMapping(value = "delete", method = RequestMethod.GET)
    public ModelAndView deleteUsers(@RequestParam String id) {
        LoginInfo obj = (LoginInfo) comDao.getBeanById(LoginInfo.class, Integer.parseInt(id));
        if(obj!=null)
            comDao.delete(obj.getUser());
        List listAll = comDao.listAll(LoginInfo.class);
        ModelAndView model = new ModelAndView("userslist"); //return page
        model.addObject("userslist", listAll);
        return model;
    }

}
