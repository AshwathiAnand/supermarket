/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supermarket.controller;

import com.sun.net.httpserver.HttpServer;
import com.supermarket.bean.LoginInfo;
import com.supermarket.bean.UserInfo;
import com.supermarket.dao.CommonDao;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author ashwa
 */
//This class acts as servlet
@Controller
@SessionAttributes("LB")
public class IndexController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String sayHello(ModelMap model) {
        return "index";    //returns index.jsp page. Actually it is returning String "index" to 
                           //the AppConfig.java pages viewResolver(). It is from there the page is returned.
    }
    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public String logout(ModelMap model,SessionStatus status, HttpSession session) {
        status.setComplete();
        session.invalidate();
        return "index";    //returns index.jsp page. Actually it is returning String "index" to 
                           //the AppConfig.java pages viewResolver(). It is from there the page is returned.
    }
    
    @Autowired
    CommonDao comDao;
    @RequestMapping(value = "signin", method = RequestMethod.POST)
    public ModelAndView login(@ModelAttribute ("login") LoginInfo login, HttpSession session) {
        String[] key ={"email", "password"};
        LoginInfo obj = (LoginInfo) comDao.getUniqueObject(login.getClass(), key, login.getEmail(), login.getPassword());//inorder
        //to identify the db table login.getClass() is called, inwhich the mapping is done.
        
        ModelAndView mv = new ModelAndView("index");
        if(obj != null) {
           if(obj.getType().equals("A")){
                mv.setViewName("adminHome");
                session.setAttribute("LB", obj);
            } else if(obj.getType().equals("C")) {
                 mv.setViewName("userHome");
                 session.setAttribute("LB", obj);
            }  
        }   
        
       return mv;  //returns index.jsp page 
    }
}
