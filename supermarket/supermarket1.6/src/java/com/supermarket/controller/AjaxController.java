package com.supermarket.controller;

import com.supermarket.bean.LoginInfo;
import com.supermarket.bean.ProductInfo;
import com.supermarket.bean.UserInfo;
import com.supermarket.dao.CommonDao;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//For implementing REST API RestController is used
@RestController
public class AjaxController {

    @Autowired
    CommonDao comDAO;
    
    //  SEARCH box
    @RequestMapping(value = "gosearch", method = RequestMethod.GET, produces = "application/json")
    public String go(@RequestParam("id") String name) {
        JSONObject obj = new JSONObject();
        obj.put("name", name);
        return obj.toJSONString();
    }
 /* @RequestMapping(value = "go", method = RequestMethod.GET, produces = "application/json")
    public String go(@ModelAttribute("login") LoginInfo login, @ModelAttribute("register") UserInfo register) {
        register.setLoginInfo(login);
        comDAO.save(register);
        JSONObject obj = new JSONObject();
        obj.put("hello", "haiii");
        return obj.toJSONString();
    }*/
    
  /*  @RequestMapping(value = "go", method = RequestMethod.GET, produces = "application/json")
    public String goUsingGET(@ModelAttribute("login") LoginInfo login, @ModelAttribute("register") UserInfo register) {

        JSONObject obj = new JSONObject();
       
        LoginInfo uniqueObject = (LoginInfo) comDAO.getUniqueObject(LoginInfo.class, new String[]{"email", "password"}, login.getEmail(), login.getPassword());
        
        obj.put("email", login.getEmail());
        obj.put("password", login.getPassword());

        return obj.toJSONString();   
    }
  */
  /*@RequestMapping(value = "go", method = RequestMethod.GET, produces = "application/json")
    public String goUsingGET(@ModelAttribute("login") LoginInfo login, @ModelAttribute("register") UserInfo register) {
//        register.setLoginInfo(login);
//        comDAO.save(register);
        JSONObject obj = new JSONObject();
        obj.put("response", login.getEmail());
        System.out.println(name);
        
        Object uniqueObject = comDAO.getUniqueObject(LoginInfo.class, new String[]{"email"}, name);
        obj.put("name", "");
        if (uniqueObject != null) {
            obj.put("name", "already exists");
        }
        return obj.toJSONString();    //returns index.jsp page. Actually it is returning String "index" to 
        //the AppConfig.java pages viewResolver(). It is from there the page is returned.
       // return login.toJSON().toJSONString();
    }*/
    

 /*   @RequestMapping(value = "go", method = RequestMethod.POST, produces = "application/json")
    public String go(@ModelAttribute("login") LoginInfo login, @ModelAttribute("register") UserInfo register) {
        register.setLoginInfo(login);
        comDAO.save(register);
       return login.toJSON().toJSONString();
    }*/

    
    

}
