<%-- 
    Document   : userslist
    Created on : Aug 29, 2018, 12:27:33 PM
    Author     : ashwa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %><!--for adding expression to jsp page taglib tag is used -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <tr>
                <th>ID</th>
                <th>NAME</th>
                <th>EMAIL</th>
                <th>PASSWORD</th>
                <th>PHONE NUMBER</th>
                <th>GENDER</th>
                <th>ADDRESS</th>
                <th>ACTION</th>
            </tr>
            <c:if test="${not empty userslist}">    <!--If there is user, then one by one user details are retrieved -->
                <c:forEach items="${userslist}" var="u">
                    <tr>
                        <td>
                            ${u.getId()}
                        </td>
                        <td>
                            ${u.getUser().getName()}    <!--As we have set login as parent bean, 
                                                        wants to access data using getUser().method. ie.,indirect accessing-->
                        </td>
                        <td>
                            ${u.getUser().getEmail()}
                        </td>
                        <td>
                            ${u.getPassword()}
                        </td>
                        <td>
                            ${u.getUser().getPhone()}
                        </td>
                        <td>
                            ${u.getUser().getGender()}
                        </td>
                        <td>
                            ${u.getUser().getAddress()}
                        </td>
<!--                        <td>
                            <a href="edit?id=${u.getId()}">Edit</a>
                        </td>
                        <td>
                            <a href="delete?id=${u.getId()}">Delete</a>
                        </td>-->
                     </tr>   
                </c:forEach>
            </c:if>
            
        </table>
        
    </body>
</html>
