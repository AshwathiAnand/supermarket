<%-- 
    Document   : adminAddCategory
    Created on : Sep 6, 2018, 2:40:26 PM
    Author     : ashwa
--%>
<%@include file="adminHeader.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%@taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %><!--for adding expression to jsp page taglib tag is used -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin View Category Page</title>
    </head>
   <body>
        <table class="table table-striped table-hover">
            <tr>
                <th>ID</th>
                <th>CATEGORY_NAME</th>
                <th>ACTION</th>
            </tr>
            <c:if test="${not empty categorylist}">    <!--If there is user, then one by one user details are retrieved -->
                <c:forEach items="${categorylist}" var="ca">
                    <tr>
                        <td>
                            ${ca.getId()}
                        </td>
                        <td>
                            ${ca.getName()}    <!--As we have set login as parent bean, 
                                                        wants to access data using getUser().method. ie.,indirect accessing-->
                        </td>
                        <!--<td>
                            <a href="edit?id=${ca.getId()}">Edit</a>
                        </td>
                        <td>
                            <a href="delete?id=${ca.getId()}">Delete</a>
                        </td>-->
                     </tr>   
                </c:forEach>
            </c:if>
            
        </table>
        
    </body>
</html>
<%@include file="adminFooter.jsp" %>

