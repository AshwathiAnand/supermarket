<%-- 
    Document   : adminAddCategory
    Created on : Sep 6, 2018, 2:40:26 PM
    Author     : ashwa
--%>
<%@include file="adminHeader.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%@taglib prefix = "adm" uri="http://java.sun.com/jsp/jstl/core" %><!--for adding expression to jsp page taglib tag is used -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin View Products Page</title>
    </head>
   <body>
        <table class="table table-striped table-hover">
            <tr>
                <th>ID</th>
                <th>PRODUCT</th>
                <th>CATEGORY</th>
                <th>DESCRIPTION</th>
                <th>RATE</th>
                <th>IMAGE</th>
                <th>STOCK QTY (IN MEASUREMENT)</th>
                <th>AVAILABILITY</th>
                <th>ACTION</th>
            </tr>
            <adm:if test="${not empty productlist}">    <!--If there is user, then one by one user details are retrieved -->
                <adm:forEach items="${productlist}" var="p">
                    <tr>
                        <td>
                            ${p.getId()}
                        </td>
                        <td>
                            ${p.getName()}    <!--As we have set login as parent bean, 
                                                        wants to access data using getUser().method. ie.,indirect accessing-->
                        </td>
                        <td>
                            ${p.getCategoryInfo().getName()}

                        </td>
                        <td>
                            ${p.getDescription()}

                        </td>
                        <td>
                            ${p.getPrice()}
                        </td>
                        <td>
                            <img src ="resources/tmp/${p.getName()}/${p.getPhoto()}" width="50px" height="50px"/>
                        </td>
                        <td>
                            ${p.getStock_qty()}
                            ${p.getQty_measurement()}
                        </td>
                        <td>
                            ${p.getAvailability()}
                        </td>
                        <td>
                            <a href="editproduct?id=${p.getId()}">Edit</a>
                        </td>
                        <td>
                           <a href="deleteproduct?id=${p.getId()}">Delete</a>
                        </td>
                     </tr>   
                </adm:forEach>
            </adm:if>
            
        </table>
        
    </body>
</html>
<%@include file="adminFooter.jsp" %>

