<%-- 
    Document   : register
    Created on : Aug 9, 2018, 1:55:09 PM
    Author     : ashwa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration Page</title>
    </head>
    <body>
        <h1>${message}</h1> <!--ModelMapping -->
        <form action="saveuser" method="post" enctype="multipart/form-data">
            <label>Name :</label><input type="text" name="name" /><br/><br/>
            <label>Username :</label><input type="text" name="username" /><br/><br/>
            <label>Password :</label><input type="password" name="password" /><br/><br/>
            <label>Confirm :</label><input type="password" name="cpass" /><br/><br/>
            <label>Phone :</label><input type="number" name="phone" /><br/><br/>
            <label>Email :</label><input type="email" name="email" /><br/><br/>
            <label>File :</label><input type="file" name="file" /><br/><br/>
            <label></label><input type="submit" value="submit" /><br/><br/>
        </form>
        <style>
            label {
                width: 100px;
                display: inline-block;
            }            
        </style>
    </body>
</html>
