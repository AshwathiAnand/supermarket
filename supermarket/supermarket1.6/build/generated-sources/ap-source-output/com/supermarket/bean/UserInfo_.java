package com.supermarket.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserInfo.class)
public abstract class UserInfo_ {

	public static volatile SingularAttribute<UserInfo, String> address;
	public static volatile SingularAttribute<UserInfo, String> gender;
	public static volatile SingularAttribute<UserInfo, String> phone;
	public static volatile SingularAttribute<UserInfo, String> name;
	public static volatile SingularAttribute<UserInfo, Integer> id;
	public static volatile SingularAttribute<UserInfo, LoginInfo> login;

}

