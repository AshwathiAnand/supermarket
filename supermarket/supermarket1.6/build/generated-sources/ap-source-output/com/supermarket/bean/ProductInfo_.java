package com.supermarket.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProductInfo.class)
public abstract class ProductInfo_ {

	public static volatile SingularAttribute<ProductInfo, String> qty_measurement;
	public static volatile SingularAttribute<ProductInfo, String> price;
	public static volatile SingularAttribute<ProductInfo, String> stock_qty;
	public static volatile SingularAttribute<ProductInfo, String> name;
	public static volatile SingularAttribute<ProductInfo, String> description;
	public static volatile SingularAttribute<ProductInfo, String> photo;
	public static volatile SingularAttribute<ProductInfo, Integer> id;
	public static volatile SingularAttribute<ProductInfo, String> availability;
	public static volatile SingularAttribute<ProductInfo, CategoryInfo> categoryInfo;

}

