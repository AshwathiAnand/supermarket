package com.supermarket.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CategoryInfo.class)
public abstract class CategoryInfo_ {

	public static volatile ListAttribute<CategoryInfo, ProductInfo> product;
	public static volatile SingularAttribute<CategoryInfo, String> name;
	public static volatile SingularAttribute<CategoryInfo, Integer> id;

}

