package com.supermarket.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(LoginInfo.class)
public abstract class LoginInfo_ {

	public static volatile SingularAttribute<LoginInfo, String> password;
	public static volatile SingularAttribute<LoginInfo, Integer> id;
	public static volatile SingularAttribute<LoginInfo, String> type;
	public static volatile SingularAttribute<LoginInfo, UserInfo> user;
	public static volatile SingularAttribute<LoginInfo, String> email;

}

