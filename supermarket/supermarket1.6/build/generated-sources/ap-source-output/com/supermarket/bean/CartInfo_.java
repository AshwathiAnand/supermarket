package com.supermarket.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(CartInfo.class)
public abstract class CartInfo_ {

	public static volatile SingularAttribute<CartInfo, String> total;
	public static volatile SingularAttribute<CartInfo, ProductInfo> product;
	public static volatile SingularAttribute<CartInfo, String> qty;
	public static volatile SingularAttribute<CartInfo, Integer> id;
	public static volatile SingularAttribute<CartInfo, String> status;

}

