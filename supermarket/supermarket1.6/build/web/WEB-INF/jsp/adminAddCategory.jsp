<%-- 
    Document   : adminAddCategory
    Created on : Sep 6, 2018, 2:40:26 PM
    Author     : ashwa
--%>
<%@include file="adminHeader.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration Page</title>
    </head>
    <body>
        <h4>${message}</h4> <!--ModelMapping -->
        <form action="addcategory" method="post">
            <h2>Add Product Category</h2>
            <label>Name :</label><input type="text" name="name" /><br/><br/>
            <label></label><input type="submit" value="submit" /><br/><br/>
        </form>
        <style>
            label {
                width: 100px;
                display: inline-block;
            }            
        </style>
    </body>
</html>
<%@include file="adminFooter.jsp" %>

