<%-- 
    Document   : adminEditProduct
    Created on : Sep 6, 2018, 2:40:26 PM
    Author     : ashwa
--%>
<%@include file="adminHeader.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%@taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Products</title>
    </head>
    <body>
        <h4>${message}</h4> <!--ModelMapping -->
        <form action="updateproduct" method="post" enctype="multipart/form-data">
            <h2>Edit Product Details</h2>
            <label>Select Category :</label>
                <select style="width:30%">
                    <c:if test="${not empty category}">
                      <c:forEach items ="${category}" var="ca"> 
                         <option selected="${editproducts.getCategoryInfo().getId()}" value="${ca.getName()}">${ca.getName()}</option>            
                     </c:forEach>
                    </c:if>
                          </select><br/><br/>
            <label>Name :</label><input style ="width:30%" type="text" name ="name" value="${editproducts.getName()}" /><br/><br/>
            <label>Description :</label><textarea style ="width:30%" name="description">${editproducts.getDescription()}</textarea><br/><br/>
            <label>Rate ( per unit) :</label><input style ="width:30%" type="text" name ="price" value="${editproducts.getPrice()}" /><br/><br/>
            <label>Stock Quantity :</label><input style ="width:30%" type="text" name ="stock_qty" value="${editproducts.getStock_qty()}"/>
                                 <select name ="qty_measurement">                      
                                        <option value="${editproducts.getQty_measurement()}">Kilogram</option>                                   
                                 </select><br/><br/>
            <label>Photo :</label><input style ="width:30%" type="file" name ="file" value="${editproducts.getName()}" /><br/><br/>
            <label>Availability of Stock :</label>
            <c:if test ="${editproducts.getAvailability() == 'yes'}">
                 <input type="radio" name ="availability" value="yes" checked="checked" />Yes&nbsp;&nbsp;&nbsp;&nbsp;
            </c:if>
           
                 <input type="radio" name="availability" value="no"  <c:if test="${editproducts.getAvailability() == 'no'}">checked="checked"  </c:if>/>No<br /><br/>
           
            <input type="submit" value="submit" style ="width:12%" /><br/><br/>
           
        </form>
        <style>
            label {
                width:30%;
                display: inline-block;
            }            
        </style>
    </body>
</html>
<%@include file="adminFooter.jsp" %>

