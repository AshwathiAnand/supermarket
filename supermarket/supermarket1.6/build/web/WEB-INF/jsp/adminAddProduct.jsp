<%-- 
    Document   : adminAddCategory
    Created on : Sep 6, 2018, 2:40:26 PM
    Author     : ashwa
--%>
<%@include file="adminHeader.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<%@taglib prefix = "c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration Page</title>
    </head>
    <body>
        <h4>${message}</h4> <!--ModelMapping -->
        <form action="addproduct" method="post" enctype="multipart/form-data">
            <h2>Add Product </h2>
            <label>Select Category :</label>
            <select style ="width:30%" name = "id">
                <c:if test="${not empty categorylist_in_product}">    <!--If there is user, then one by one user details are retrieved -->
                    <c:forEach items="${categorylist_in_product}" var="ca">
                        <option value="${ca.getId()}">${ca.getName()}    </option> 
                    </c:forEach>
                </c:if>                 
            </select><br/><br/>
            <label>Name :</label><input style ="width:30%" type="text" name ="name" /><br/><br/>
            <label>Description :</label><textarea style ="width:30%" name="description"> </textarea><br/><br/>
            <label>Rate ( per unit) :</label><input style ="width:30%" type="text" name ="price" /><br/><br/>
            <label>Stock Quantity :</label><input style ="width:30%" type="text" name ="stock_qty" />
                                 <select name ="qty_measurement">
                                    <option value="Kilogram(s)">Kilogram</option>
                                    <option value="litre(s)">litre</option>
                                 </select><br/><br/>
            <label>Photo :</label><input type="file" name ="file" /><br/><br/>
            <label>Availability of Stock :</label><input type="radio" name ="availability" value ="yes"/>Yes&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="availability" value="no" />No<br /><br/>
            <input type="submit" value="submit" /><br /><br/>
            <input type="reset" value="Cancel" />
        </form>
        <style>
            label {
                width:30%;
                display: inline-block;
            }            
        </style>
<!--    </body>
</html>-->
<%@include file="adminFooter.jsp" %>

